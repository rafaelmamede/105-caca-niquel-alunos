import java.util.Random;

public class Slot {
	Simbolos simbolos;

	public void girar() {
		Random rand = new Random();
		simbolos = Simbolos.values()[rand.nextInt(Simbolos.values().length)];
	}

	@Override
	public String toString() {
		return "Nome " + simbolos.getNome() + " Valor: " + simbolos.getValor();
	}
}
